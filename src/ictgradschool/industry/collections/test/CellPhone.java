package ictgradschool.industry.collections.test;

public class CellPhone  {
    double size;
    double price;
    String brand;
    String model;

    public CellPhone(double price, String brand, String model) {
        this.price = price;
        this.brand = brand;
        this.model = model;
    }

    public String toString() {
        return brand + " " + model + " " + price;


    }

//    @Override
//    public int compareTo(CellPhone other) {
//        int difference = Double.valueOf(other.price).compareTo(this.price);
//                if (difference == 0){
//            return this.brand.compareTo(other.brand);
//                }
//        return Double.valueOf(this.price).compareTo(other.price);
//    }
}
