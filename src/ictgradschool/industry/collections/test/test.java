package ictgradschool.industry.collections.test;


import java.util.*;

public class Test {
    public static void main(String[] args) {
//        Set<String> s = new HashSet<>();
//        s.add("Ardria");
//        s.add("Owen");
//        s.add("Mary");
//        s.add("Nick");
//        s.add("Josh");
//        s.add("Soo");
//        s.add("Vivian");
//        for (String name : s){
//            System.out.println(name);

        List<CellPhone> phones = new ArrayList<>();
        phones.add(new CellPhone(1546.65, "iPhone", "X"));
        phones.add(new CellPhone(600, "Huawei", "Mate10"));
        phones.add(new CellPhone(600, "iPhone", "X1"));

        Comparator<CellPhone> comp = new Comparator<CellPhone>() {
            @Override
            public int compare(CellPhone o1, CellPhone o2) {
                int difference = Double.valueOf(o2.price).compareTo(o1.price);
                if (difference == 0) {
                    return o2.brand.compareTo(o1.brand);
                }
                return difference;
            }
        };

        Collections.sort(phones, comp);

        for (CellPhone p : phones) {
            System.out.println(p);
        }
    }
}
